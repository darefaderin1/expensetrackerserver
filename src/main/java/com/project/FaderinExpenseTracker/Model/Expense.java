package com.project.FaderinExpenseTracker.Model;

import java.util.List;

public class Expense {

    private List<ExpenseOwner> expenseOwners;

    public List<ExpenseOwner> getExpenseOwners() {
        return expenseOwners;
    }

    public void setExpenseOwners(List<ExpenseOwner> expenseOwners) {
        this.expenseOwners = expenseOwners;
    }
}
