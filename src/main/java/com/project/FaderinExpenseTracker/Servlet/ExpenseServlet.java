package com.project.FaderinExpenseTracker.Servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.project.FaderinExpenseTracker.MessageResponse.MessageResponseTemplate;
import com.project.FaderinExpenseTracker.Model.Expense;
import com.project.FaderinExpenseTracker.Model.ExpenseItem;
import com.project.FaderinExpenseTracker.Model.ExpenseOwner;
import com.project.FaderinExpenseTracker.Model.ExpenseUser;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ExpenseServlet", value = "/ExpenseServlet")
public class ExpenseServlet extends HttpServlet {
    private final String storageFilename = "storage.json";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MessageResponseTemplate theMessageResponseTemplate = new MessageResponseTemplate();
        ObjectMapper mapper = new ObjectMapper();
        theMessageResponseTemplate.setMessage("Bad Request");
        theMessageResponseTemplate.setStatus("error");
        response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        System.out.println("POST Method...");
        String requestType = request.getParameter("requestType");
        String userName = request.getParameter("userName");
        MessageResponseTemplate theMessageResponseTemplate = new MessageResponseTemplate();
        ObjectMapper mapper = new ObjectMapper();
        if (requestType != null && requestType.equalsIgnoreCase("authenticate")) {
            String fullName = request.getParameter("fullName");
            if (isUserAuthenticated(userName)) {
                ///user is already authenticated, you can add expense now.
                theMessageResponseTemplate.setMessage("Welcome " + userName + ", you can now add a expense entry.");
                theMessageResponseTemplate.setStatus("success");
            } else {
                ///if user is not in storage and full name is
                // not provided for new user creation, prompt message below
                String msg = "user name does not exit, error occurred while creating user";
                theMessageResponseTemplate.setStatus("error");
                if (fullName == null || fullName.isEmpty() || userName.isEmpty() || userName == null) {
                    ///if full name is not provided and username
                    // does not exist, prompt error message
                    msg = "Please specify user name and full name to continue";
                }
                if (userName != null && fullName != null && !userName.isEmpty() && !fullName.isEmpty()) {
                    ///if full name and username is provided and username
                    // does not exist, create user and prompt success message
                    createUser(userName, fullName);
                    msg = "User Created, please add expenses";
                    theMessageResponseTemplate.setStatus("success");
                }
                theMessageResponseTemplate.setMessage(msg);
                ///user is not authenticated, you need to add a user with full name.
            }
            response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
            /////////////////////USER AUTHENTICATION ENDS HERE////////////////////


            ////////////////EXPENSE ENTRY BEGINS HERE//////////////////////
        } else if (requestType != null && requestType.equalsIgnoreCase("enterexpense")) {
            if (!isUserAuthenticated(userName)) {
                ////if the user trying to enter expense item does
                // not exist in storage, quit processing of data
                theMessageResponseTemplate.setMessage("User does not exist, " +
                        "please authenticate and specify a username before entering expense item");
                theMessageResponseTemplate.setStatus("error");
                response.setStatus(400);
                response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
                return;
            }
            String itemName = request.getParameter("itemName");
            double amount;
            try {
                amount = Double.parseDouble(request.getParameter("amount"));
            } catch (Exception e) {
                System.out.println("Can not get Amount from Request");
                ////send a message to the client and stop processing the request
                theMessageResponseTemplate.setMessage("Please specify amount before entering expense item");
                theMessageResponseTemplate.setStatus("error");
                response.setStatus(400);
                response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
                return;
            }
            if (itemName == null || itemName.isEmpty()) {
                theMessageResponseTemplate.setMessage("Please specify item name before entering expense item");
                theMessageResponseTemplate.setStatus("error");
                response.setStatus(400);
                response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
                return;
            }
            /////if all goes well, create new expense class
            // which will be added to an existing expense owner class
            ExpenseItem expenseItem = new ExpenseItem();
            ////add amount to newly created expense
            expenseItem.setItemAmount(amount + "");
            /////add item name to newly created expense
            expenseItem.setItemName(itemName);
            /////create current date from machine date and time
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            /////set created date as expense date
            expenseItem.setDate(dtf.format(now));
            ////get expenses from a predefined method so we can retrieve the correct expense owner
            Expense expense = getExpenses();
            ///get expense owner from expense class with its corresponding location
            ExpenseOwner updatedOwner = new ExpenseOwner();
            ////store owner location, owner location is set to -1 because,
            // valid arrays begins from 0. we need to look for
            // the expense owner in the storage so we can modify
            int existingExpenseOwnerLocation = -1;
            /////look through existing expense owners to find the current user data
            List<ExpenseOwner> owners = expense.getExpenseOwners();
            for (int i = 0; i < owners.size(); i++) {
                if (owners.get(i).getUserName().equalsIgnoreCase(userName)) {
                    updatedOwner = owners.get(i);
                    existingExpenseOwnerLocation = i;
                    /////when the expense owner location is found, we have to stop
                    // and make the found index the new expense owner location
                    break;
                }
            }
            ////for the current owner, add newly created expense item to the existing one
            List<ExpenseItem> expenseItems = updatedOwner.getExpenseItems();
            if (expenseItems == null) {
                expenseItems = new ArrayList<>();
            }
            expenseItems.add(expenseItem);
            updatedOwner.setExpenseItems(expenseItems);
            System.out.println("Expense Item Added");
            ////at this point, ExpenseOwner, alias owner now contains
            // newly created expense, we need to replace the new owner
            // data with the existing one in the expense owner list
            owners.remove(existingExpenseOwnerLocation);
            ////update expense owners at this point by adding
            // newly modified owner data. Note that we removed
            // old owner from the list to avoid duplicate
            owners.add(updatedOwner);
            /////replace old expense owners with the modified expense owner
            expense.setExpenseOwners(owners);
            ///call a predefined method to update expense
            updateExpense(expense);
            /////send a message to the client
            // informing that expense item has been added
            theMessageResponseTemplate.setStatus("success");
            theMessageResponseTemplate.setMessage("New Expense item successfully added for: " + userName);
            response.setStatus(200);
            response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
        }
        ////////////////EXPENSE ENTRY ENDS HERE//////////////////////
        else ////////////////GET USERS BEGINS HERE//////////////////////
            if (requestType != null && requestType.equalsIgnoreCase("getAllUsers")) {
                System.out.println("Getting all users...");
                List<ExpenseUser> allUsers = new ArrayList<>();
                Expense expenseStorage = getExpenses();
                List<ExpenseOwner> allExpenseOwner = expenseStorage.getExpenseOwners();
                if (allExpenseOwner == null) {
                    allExpenseOwner = new ArrayList<>();
                }
                int serialNumber = 1;
                for (ExpenseOwner owner : allExpenseOwner) {
                    ExpenseUser user = new ExpenseUser();
                    user.setUserName(owner.getUserName());
                    user.setFullName(owner.getFullName());
                    user.setSN(serialNumber + "");
                    serialNumber++;
                    allUsers.add(user);
                }
                theMessageResponseTemplate.setResponse(allUsers);
                theMessageResponseTemplate.setStatus("success");
                theMessageResponseTemplate.setMessage("All Expense Users");
                response.setStatus(200);
                response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
            }
            ////////////////GET USERS ENDS HERE//////////////////////


            ////////////////GET EXPENSE BY USER NAME BEGINS HERE//////////////////////
            else if (requestType != null && requestType.equalsIgnoreCase("getAllExpenseByUser")) {
                if (userName == null || userName.isEmpty()) {
                    theMessageResponseTemplate.setStatus("error");
                    theMessageResponseTemplate.setMessage("Please specify username before you can continue.");
                    response.setStatus(400);
                } else {
                    ExpenseOwner owner = getExpenseOwner(userName);
                    List<ExpenseItem> allExpense = owner.getExpenseItems();
                    if (allExpense == null) {
                        allExpense = new ArrayList<>();
                    }
                    theMessageResponseTemplate.setResponse(allExpense);
                    theMessageResponseTemplate.setStatus("success");
                    theMessageResponseTemplate.setMessage("All Expenses for: " + userName);
                    response.setStatus(200);
                }
                response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
            } else {
                theMessageResponseTemplate.setMessage("Bad Request");
                theMessageResponseTemplate.setStatus("error");
                response.setStatus(400);
                response.getWriter().write(mapper.writeValueAsString(theMessageResponseTemplate));
            }

    }

    public boolean isUserAuthenticated(String userName) {
        ///set authenticated to be false by default
        boolean userIsAuthenticated = false;
        ////get expense owner from an existing method using username
        ExpenseOwner owner = getExpenseOwner(userName);
        ////check if the returned data has a username, i.e if username is null, that means the owner was not found
        if (owner.getUserName() != null && owner.getUserName().equalsIgnoreCase(userName)) {
            ////if the username matches returned expense owner, it means the owner already exist in the record
            userIsAuthenticated = true;
        }
        return userIsAuthenticated;
    }

    public void createUser(String userName, String fullName) {
        System.out.println("Creating user...");
        ExpenseOwner owner = new ExpenseOwner();
        owner.setUserName(userName);
        owner.setFullName(fullName);
        ////Get Expenses from storage
        System.out.println("Before get expense");
        Expense expense = getExpenses();
        System.out.println("After get expense");
        ////Get list of existing expense owners so we can add the newly created owner to the list
        List<ExpenseOwner> owners = expense.getExpenseOwners();
        if (owners == null) {
            owners = new ArrayList<>();
        }
        System.out.println("Expense owner size: " + owners.size());
        ////add newly created owner to existing list
        owners.add(owner);
        /////replace new owners with the existing ones using the set method
        expense.setExpenseOwners(owners);
        ////call a predefined method to update the expense in the storage file.
        updateExpense(expense);
    }

    public ExpenseOwner getExpenseOwner(String username) {
        ExpenseOwner owner = new ExpenseOwner();
        Expense expense = getExpenses();
        List<ExpenseOwner> owners = expense.getExpenseOwners();
        if (owners != null) {
            for (int i = 0; i < owners.size(); i++) {
                if (owners.get(i).getUserName().equalsIgnoreCase(username)) {
                    return owners.get(i);
                }
            }
        } else {
            System.out.println("No user registered yet.");
        }
        return owner;
    }

    private void updateExpense(Expense expense) {
        try {
            ////look for storage file on the machine
            ServletContext context = getServletContext();
            String path = context.getRealPath(storageFilename);
            File file = new File(path);
            ///check if file exist in the location
            if (!file.isFile()) {
                ///if it doesnt exist, create a new file with the name storage.txt
                initializeExpenseStorage(file);
            }

            ///Create object mapper here... this is an instance of Jackson JSON
            ObjectMapper mapper = new ObjectMapper();
            ////use an inbuilt jackson json to beautify the output
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            ///convert the class to a JSON string for storage
            String jsonData = mapper.writeValueAsString(expense);
//            System.out.println("Data to store: " + jsonData);
            mapper.writeValue(file, expense);
        } catch (Exception e) {
            System.out.println("could not update expense because: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private Expense getExpenses() {
        ///create an empty expense class as a placeholder
        Expense expense = new Expense();
        try {
            ////look for storage file on the machine
            ServletContext context = getServletContext();
            String path = context.getRealPath(storageFilename);

            File file = new File(path);
            ///check if file exist in the location
            if (!file.isFile()) {
                ///if it doesnt exist, create a new file with the name storage.txt
                initializeExpenseStorage(file);
            } else {
                ////use jackson object mapper to read the file which already contains a json and convert to expense class
                ObjectMapper mapper = new ObjectMapper();
                ////replace the expense placeholder with the file content
                expense = mapper.readValue(file, Expense.class);
                ////if reading file content fail, replace expense with placeholder and return
            }
        } catch (Exception e) {
            System.out.println("File could not be loaded or mapped to json class: " + e.getMessage() + " the expense class has been resolved to a new instance");
            return new Expense();
            ////Expense data can not be read from file, file could be empty for a first time, so we return new instance of expense
        }
        ////if file doesnt exist already, return an empty expense placeholder class
        return expense;
    }

    private void initializeExpenseStorage(File file) {
        try {
            file.createNewFile();
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.writeValue(file, new Expense());
        } catch (IOException e) {
            System.out.println("Could not create file and write content because: " + e.getMessage());
        }
    }

}
